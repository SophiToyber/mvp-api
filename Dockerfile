FROM openjdk:8

COPY build/libs/*.jar mvp-api.jar

EXPOSE 8184 8185

ENTRYPOINT [ "java", "-jar", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:8185", "mvp-api.jar"]
