package mvp.api.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import mvp.api.dto.CityDTO;
import mvp.api.dto.CountryDTO;
import mvp.api.dto.HotelDTO;

@Slf4j
@Service
@SuppressWarnings("unused")
public class HotelService {

    public void sendToHotels(HotelDTO hotel) throws IOException {
        try {
            URL url = new URL("http://localhost:8080/hotels");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);
            
            log.info("{}", accert(hotel));
            ObjectMapper mapper = new ObjectMapper();

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = mapper.writeValueAsBytes(hotel).toString().getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void sendToCountry(CountryDTO country) throws IOException {
        try {
            URL url = new URL("http://localhost:8080/country");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);
            ObjectMapper mapper = new ObjectMapper();

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = mapper.writeValueAsBytes(country).toString().getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void sendToCity(CityDTO city) throws IOException {
        try {
            URL url = new URL("http://localhost:8080/cities");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);
            ObjectMapper mapper = new ObjectMapper();

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = mapper.writeValueAsBytes(city).toString().getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
    
    public static boolean accert (Object obj) {
        return !obj.equals(null);
    }

}
